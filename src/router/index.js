import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from "../components/Home";
import Login from "../components/Login";
import Device from "../components/Device"
import Report from "../components/Report";
import Chart from "../components/Chart";
import Monitor from "../components/Monitor";
import About from "../components/About"
import Welcome from "../components/Welcome";
import DeviceList from "../components/DeviceList";

Vue.use(VueRouter)

const routes = [
  { path: '/', redirect: '/login' },
  { path: '/login', component: Login },
  {
    path: '/home',
    component: Home,
    redirect: '/device',
    children: [
      { path: '/welcome', component: Welcome },
      { path: '/history-report', component: Report },
      { path: '/history-chart', component: Chart },
      { path: '/device', component: Device},
      { path: '/monitor', component: Monitor},
      { path: '/deviceList', component: DeviceList},
      { path: '/about-us', component: About}
    ]
  }
]

const router = new VueRouter({
  routes,
  mode:'history'
})

export default router
